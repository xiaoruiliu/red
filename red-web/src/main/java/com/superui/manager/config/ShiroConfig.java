package com.superui.manager.config;

import com.superui.manager.shiro.CustomSessionManager;
import com.superui.manager.shiro.CustomRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @ClassName ShiroConfig
 * @Path com/superui/manager/config/ShiroConfig.java
 * @Author rui
 * @Description shiro 配置文件
 * @Date 14:43 2022/8/29
 **/
@Configuration
public class ShiroConfig {

    //角色权限控制
    @Bean
    public ShiroFilterFactoryBean shirFilter(){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager());
        /*//登陆URL
        shiroFilterFactoryBean.setLoginUrl("/gotoLogin.html");
        //欢迎页面URL
        shiroFilterFactoryBean.setSuccessUrl("/vue2/index.html#/dashboard");
        shiroFilterFactoryBean.setUnauthorizedUrl("/401.html");*/

        shiroFilterFactoryBean.setFilterChainDefinitionMap(getFilterChainDefinitionMap());
        return shiroFilterFactoryBean;
    }

    private Map<String,String> getFilterChainDefinitionMap(){
        //Filter的执行有一定顺序，应该使用LinkedHashMap，使用HashMap会出问题
        Map<String,String> map = new LinkedHashMap<>();
        map.put("/test/**", "anon");
        map.put("/cache/**", "anon");

        /*authc:所有url都必须认证通过才可以访问;
        anon:所有url都都可以匿名访问*/
        map.put("/**", "authc");
        return map;
    }

    //会话控制 sessionManager,(shiro可以同时管理多个应用的登陆)


    /**
     * 配置SecurityManager
     * @return SecurityManager
     */
    @Bean
    public SecurityManager securityManager(){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        //如果前后端没有分离，则不必使用sessionManager，不必设置SessionManager相关配置
        securityManager.setSessionManager(sessionManager());

        //使用自定义的CacheManager
        securityManager.setCacheManager(cacheManager());

        //设置Realm。建议放到最后，否则可能会出问题
        securityManager.setRealm(customRealm());

        return securityManager;
    }

    /**
     * 获取自定义的sessionManager
     * @return SessionManager
     */
    @Bean
    public SessionManager sessionManager(){

        CustomSessionManager customSessionManager = new CustomSessionManager();

        //设置session超时时间，单位ms。默认30min
        //customSessionManager.setGlobalSessionTimeout(20000);

        //配置session持久化
        customSessionManager.setSessionDAO(redisSessionDAO());

        return customSessionManager;
    }

    /**
     * 获取RedisSessionDAO
     * @return RedisSessionDAO
     */
    public RedisSessionDAO redisSessionDAO(){
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        redisSessionDAO.setRedisManager(redisManager());
        return redisSessionDAO;
    }

    /**
     * 获取redisManager
     * @return RedisManager
     */
    public RedisManager redisManager(){
        RedisManager redisManager = new RedisManager();
        redisManager.setHost("127.0.0.1:6379");  //设置redis服务器的地址，默认就是"127.0.0.1:6379"
        return redisManager;
    }

    /**
     * 获取RedisCacheManager
     * @return RedisCacheManager
     */
    public RedisCacheManager cacheManager(){
        RedisCacheManager redisCacheManager = new RedisCacheManager();

        redisCacheManager.setRedisManager(redisManager());

        //设置授权信息对应的key的过期时间，单位s。用户权限有变化时，除了要更新数据库，还需要更新redis中对应的key
        redisCacheManager.setExpire(24*60*60*7);

        return redisCacheManager;
    }

    /**
     * 获取自定义的Realm
     * @return CustomRealm
     */
    @Bean
    public CustomRealm customRealm(){
        CustomRealm customRealm = new CustomRealm();
        customRealm.setCredentialsMatcher(hashedCredentialsMatcher());
        return customRealm;
    }


    /**
     * 设置加密、解密规则
     * @return HashedCredentialsMatcher
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();

        //设置使用的散列算法是md5
        credentialsMatcher.setHashAlgorithmName("md5");

        //设置散列次数，即多重加密次数
        credentialsMatcher.setHashIterations(2);

        return credentialsMatcher;
    }



    /**
     * 作用：管理shiro中一些bean的生命周期，即bean初始化、销毁
     * @return LifecycleBeanPostProcessor
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }


    //以下2个bean是使用shiro的注解需要的

    /**
     * 作用：加入注解的使用，不加入这个AOP注解不生效(shiro的注解 例如 @RequiresGuest)
     * @return AuthorizationAttributeSourceAdvisor
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor()
    {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new
                AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
        return authorizationAttributeSourceAdvisor;
    }


    /**
     * 作用: 扫描上下文寻找所有的Advistor, 将符合条件的Advisor应用到切入点的Bean中
     * 这个bean要在LifecycleBeanPostProcessor创建后才可以创建
     * @return RefaultAdvisorAutoProxyCreator
     */
    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator=new
                DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setUsePrefix(true);
        return defaultAdvisorAutoProxyCreator;
    }

}
