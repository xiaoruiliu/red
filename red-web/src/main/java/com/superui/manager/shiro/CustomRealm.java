package com.superui.manager.shiro;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Set;

public class CustomRealm extends AuthorizingRealm {

    //权限管理
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取主体标识
        String username = (String) principalCollection.getPrimaryPrincipal();

        //使用dao层，根据主体标识查询用户对应的角色、权限，此处略过
        Set<String> permissions = null;
        Set<String> roles = null;//根据用户信息，拿到用户的角色信息（角色编码）

        //设置并返回主体的授权信息
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.setRoles(roles);//主要设置roles
        simpleAuthorizationInfo.setStringPermissions(permissions);

        return simpleAuthorizationInfo;
    }

    /**
     * 验证用户
     * <p>
     * 如果身份验证失败请捕获 AuthenticationException 或其子类，常见的如:
     * DisabledAccountException(禁用的帐号)、
     * LockedAccountException(锁定的帐号)、
     * UnknownAccountException(错误的帐号)、
     * ExcessiveAttemptsException(登录失败次数过 多)、
     * IncorrectCredentialsException (错误的凭证)、
     * ExpiredCredentialsException(过期的 凭证)等
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //获取主体标识
        String username = (String) token.getPrincipal();

        //使用dao层，根据主体标识查询用户正确的密码，此处略过
        String password = null;

        //如果账号、密码不匹配，返回null
        if (StringUtils.isBlank(password)){
            return null;
        }

        //设置并返回主体的认证信息，主体标识、凭证、realm的名称
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(username, password, this.getName());
        return simpleAuthenticationInfo;
    }
}
