package com.superui.manager.controller;


import com.superui.manager.dao.entity.DbConfig;
import com.superui.manager.dao.entity.UserInfo;
import com.superui.manager.service.DbConfigSv;
import com.superui.manager.service.UserInfoSv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/test")
public class UserInfoController {

    @Autowired
    private UserInfoSv userInfoSv;
    @Autowired
    private DbConfigSv dbConfigSv;

    @PostMapping(value = "add")
    @ResponseBody
    public UserInfo addEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.add(userInfo);
    }

    @PostMapping(value = "update")
    @ResponseBody
    public UserInfo updateEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.update(userInfo);
    }

    @PostMapping(value = "get")
    @ResponseBody
    public UserInfo getEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.get(userInfo);
    }

    @GetMapping(value = "del")
    public void del(Long id){
        userInfoSv.del(id);
    }

    @GetMapping("getById")
    @ResponseBody
    public UserInfo getById(Long id){
        return userInfoSv.getById(id);
    }

    @GetMapping("getCache")
    @ResponseBody
    public String getCaches(){
        return userInfoSv.getCaches();
    }

    @GetMapping("redisTst")
    @ResponseBody
    public String redisTst(String uniCode,String redisUrl,String passWord,String key){
        return userInfoSv.redisTst(uniCode,redisUrl,passWord,key);
    }

    @GetMapping("listDbConfig")
    @ResponseBody
    public List<DbConfig> listDbConfig(){
        return dbConfigSv.listAll();
    }
}
