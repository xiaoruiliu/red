package com.superui.manager.controller;

import com.superui.manager.dao.entity.UserInfo;
import com.superui.manager.service.UserInfoSv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*Spring 的cache的测试（增删改查）*/
@RestController
@RequestMapping(value = "/cache")
public class CachetestController {

    @Autowired
    private UserInfoSv userInfoSv;

    @PostMapping(value = "add")
    @ResponseBody
    public UserInfo addEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.add(userInfo);
    }

    @PostMapping(value = "update")
    @ResponseBody
    public UserInfo updateEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.update(userInfo);
    }

    @PostMapping(value = "get")
    @ResponseBody
    public UserInfo getEntity(@RequestBody UserInfo userInfo){
        return userInfoSv.get(userInfo);
    }

    @GetMapping(value = "del")
    public void del(Long id){
        userInfoSv.del(id);
    }

    @GetMapping("getById")
    @ResponseBody
    public UserInfo getById(Long id){
        return userInfoSv.getById(id);
    }

    @GetMapping("getCache")
    @ResponseBody
    public String getCaches(){
        return userInfoSv.getCaches();
    }

}
