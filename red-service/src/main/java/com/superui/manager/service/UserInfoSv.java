package com.superui.manager.service;

import com.alibaba.fastjson.JSON;
import com.superui.manager.base.service.BaseSv;
import com.superui.manager.dao.UserInfoMapper;
import com.superui.manager.dao.entity.UserInfo;
import com.superui.manager.dao.entity.UserInfoExample;
import com.superui.manager.dynamic.Aids;
import com.superui.manager.utils.ApplicationContextUtil;
import com.superui.manager.utils.redisUtils.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Service
@Aids("report")
public class UserInfoSv extends BaseSv<UserInfoMapper, UserInfo, Long, UserInfoExample> {

    @Autowired
    private RedisClient redisClient;

    //private static Logger log = LoggerFactory.getLogger(UserInfoSv.class);
    @CachePut(value = "info", key = "#userInfo.getUserName().concat('.').concat(#userInfo.getPassWord())")
    public UserInfo add(UserInfo userInfo){
        super.mapper.insert(userInfo);
        return userInfo;
    }

    @CachePut(value = "info",key = "#userInfo.getUserName().concat('.').concat(#userInfo.getPassWord())")
    public UserInfo update(UserInfo userInfo){
        super.mapper.updateByPrimaryKey(userInfo);
        return super.mapper.selectByPrimaryKey(userInfo.getId());
    }

    @Cacheable(value = "info",key = "#userInfo.getUserName().concat('.').concat(#userInfo.getPassWord())")
    public UserInfo get(UserInfo userInfo){
        UserInfoExample exmp = new UserInfoExample();
        UserInfoExample.Criteria cs = exmp.createCriteria();
        cs.andUserNameEqualTo(userInfo.getUserName());
        cs.andPassWordEqualTo(userInfo.getPassWord());
        List<UserInfo> list = super.mapper.selectByExample(exmp);
        if (!CollectionUtils.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    @CacheEvict(value = "info", allEntries = true)
    public void del(Long id){
        super.mapper.deleteByPrimaryKey(id);
    }

    @Cacheable(cacheNames = "userInfo" , key = "#id")
    public UserInfo getById(Long id) {
        //log.error("获取对象，ID="+id);
        /*DynamicDataSourceContextHolder.setCenter("oracle");
        UserInfo userInfo = super.mapper.selectByPrimaryKey(id);
        DynamicDataSourceContextHolder.clearCenter();*/
        UserInfo userInfo = super.mapper.selectByPrimaryKey(id);
        return userInfo;
    }

    /**
     * @Author rui
     * @Description redis客户端测试
     * @Date 15:02 2022/8/12
     * @Param [uniCode：redis连接的唯一比编码, redisUrl, passWord, key]
     * @return java.lang.String
     **/
    public String redisTst(String uniCode, String redisUrl, String passWord,String key) {
        return  redisClient.get(uniCode,redisUrl,passWord,key);
    }

    public String getCaches() {
        CacheManager cacheManager = ApplicationContextUtil.getBean(CacheManager.class);
        Cache cache = cacheManager.getCache("userInfo");
        Cache.ValueWrapper cacheValue = cache.get(27L);
        if (cacheValue != null){
            UserInfo userInfo = (UserInfo) cacheValue.get();
            return JSON.toJSONString(userInfo);
        }
        return "";
    }

}
