package com.superui.manager.service;

import com.superui.manager.dao.DbConfigMapper;
import com.superui.manager.dao.entity.DbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DbConfigSv {

    @Autowired
    private DbConfigMapper dbConfigMapper;

    public List<DbConfig> listAll(){
        return dbConfigMapper.listAll();
    }
}
