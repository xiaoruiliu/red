package com.superui.manager.service.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserInfoReq {

	private Long id;

	@NotNull(message = "userName不能为空")
	//@Length(max=20, message = "userName不能超过20个字节")
	private String userName;

	@NotNull(message = "passWord不能为空")
	//@Length(max=20, message = "passWord不能超过20个字节")
	private String passWord;

}