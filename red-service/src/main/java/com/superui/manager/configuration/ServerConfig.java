package com.superui.manager.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.superui.manager.dynamic.DynamicDataSource;
import com.superui.manager.dynamic.DynamicDataSourceListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy
@ComponentScan({"com.superui.manager"})
public class ServerConfig {

    /**
     * <!-- 基本属性 url、user、password -->
     * <property name="url" value="${jdbc_url}" />
     * <property name="username" value="${jdbc_user}" />
     * <property name="password" value="${jdbc_password}" />
     * <p>
     * <!-- 配置初始化大小、最小、最大 -->
     * <property name="initialSize" value="5" />
     * <property name="minIdle" value="10" />
     * <property name="maxActive" value="20" />
     * <p>
     * <!-- 配置获取连接等待超时的时间 -->
     * <property name="maxWait" value="60000" />
     * <p>
     * <!-- 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 -->
     * <property name="timeBetweenEvictionRunsMillis" value="2000" />
     * <p>
     * <!-- 配置一个连接在池中最小生存的时间，单位是毫秒 -->
     * <property name="minEvictableIdleTimeMillis" value="600000" />
     * <property name="maxEvictableIdleTimeMillis" value="900000" />
     *
     * <property name="validationQuery" value="select 1" />
     * <property name="testWhileIdle" value="true" />
     * <property name="testOnBorrow" value="false" />
     * <property name="testOnReturn" value="false" />
     *
     * <property name="keepAlive" value="true" />
     * <property name="phyMaxUseCount" value="100000" />
     * <p>
     * <!-- 配置监控统计拦截的filters -->
     * <property name="filters" value="stat" />
     *
     * @return
     */
    public DataSource dataSource() throws SQLException {
        System.setProperty("druid.mysql.usePingMethod", "false");
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://172.16.179.128:3306/rui?allowMultiQueries=true&useUnicode=true&zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai&useAffectedRows=true");
        dataSource.setUsername("root");
        dataSource.setPassword("Rui#123456");
        dataSource.setInitialSize(5);
        dataSource.setMinIdle(10);
        dataSource.setMaxActive(10);
        dataSource.setMaxWait(60000);
        dataSource.setTimeBetweenEvictionRunsMillis(2000);
        dataSource.setMinEvictableIdleTimeMillis(600000);
        dataSource.setMaxEvictableIdleTimeMillis(900000);
        dataSource.setValidationQuery("select 1");
        dataSource.setTestWhileIdle(true);
        dataSource.setTestOnBorrow(false);
        dataSource.setTestOnReturn(false);
        dataSource.setKeepAlive(true);
        dataSource.setPhyMaxUseCount(100000);
        dataSource.setFilters("stat");
        return dataSource;
    }

    @Bean
    public DataSource dynamicDataSource() throws Exception {

        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setTargetDataSources(new HashMap<Object, Object>());
        dynamicDataSource.setDefaultTargetDataSource(dataSource());
        return dynamicDataSource;
    }

    @Bean
    public DynamicDataSourceListener dynamicDataSourceListener() {
        return new DynamicDataSourceListener();
    }

    /*@Bean
    @Primary
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource) {

        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);

        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();

        PageInterceptor pageInterceptor = new PageInterceptor();
        pageInterceptor.setProperties(new Properties());
        configuration.addInterceptor(pageInterceptor);
        sessionFactory.setConfiguration(configuration);

        return sessionFactory;
    }*/

    @Bean
    public DataSourceTransactionManager transactionManager(@Autowired DataSource dataSource) {
        DataSourceTransactionManager manager = new DataSourceTransactionManager();
        manager.setDataSource(dataSource);
        return manager;
    }

    /**
     * mybatis配置, DAO扫描
     */
    /*@Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperConfig = new MapperScannerConfigurer();
        mapperConfig.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperConfig.setAnnotationClass(Mapper.class);
        mapperConfig.setBasePackage("com.reported.management.dao;com.ai.nbs.dbconfig.impl.dao;com.ai.nbs.process.impl.dao");
        return mapperConfig;
    }*/
}
