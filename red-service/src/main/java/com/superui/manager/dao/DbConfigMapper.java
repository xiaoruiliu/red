package com.superui.manager.dao;

import com.superui.manager.dao.entity.DbConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DbConfigMapper {
    List<DbConfig> listAll();
}
