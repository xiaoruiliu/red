package com.superui.manager.dao;

import com.superui.manager.base.dao.BaseMapper;

import com.superui.manager.dao.entity.UserInfo;
import com.superui.manager.dao.entity.UserInfoExample;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo, Long, UserInfoExample> {

}
