package com.superui.manager.base.dao.entity;

import java.io.Serializable;
import java.util.Date;

public abstract class MybatisEntity implements Serializable {
    public void setCreateDate(Date createDate) {}

    public void setCreatorId(Long creatorId) {}

    public void setCreatorName(String creatorName) {}

    public void setModifierId(Long modifierId) {}

    public void setModifierName(String modifierName) {}

    public void setModifierDate(Date modifierDate) {}
}
