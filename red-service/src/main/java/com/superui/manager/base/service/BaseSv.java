package com.superui.manager.base.service;

import com.superui.manager.base.dao.BaseMapper;
import com.superui.manager.base.dao.entity.MybatisEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class BaseSv<M extends BaseMapper<T, I, E>, T extends MybatisEntity, I, E> {
    /**
     * 默认mapper
     */
    protected M mapper;

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public void setMyRepository(M mapper) {
        this.mapper = mapper;
    }

    /************************基础查询*************************/
    /**
     * 得到全量数据, 甚用!
     *
     * @return
     */
    public List<T> list() {
        return mapper.selectByExample(null);
    }

    /**
     * 根据条件得到全量数据, 用于子类调用.
     *
     * @return
     */
    protected List<T> list(E exmp) {
        return mapper.selectByExample(exmp);
    }

    /**
     * 无条件分页查询
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    /*public Page<T> list(int pageNumber, int pageSize) {
        return PageHelper.startPage(pageNumber, pageSize).doSelectPage(() -> mapper.selectByExample(null));
    }*/

   /* public PageInfo<T> listPage(int pageNumber, int pageSize) {
        return new PageInfo<>(list(pageNumber, pageSize));
    }*/

    /**
     * 根据条件分页查询, 用于子类调用.
     *
     * @param pageNumber
     * @param pageSize
     * @param exmp
     * @return
     */
    /*protected Page<T> list(int pageNumber, int pageSize, E exmp) {
        return PageHelper.startPage(pageNumber, pageSize).doSelectPage(() -> mapper.selectByExample(exmp));
    }

    protected PageInfo<T> listPage(int pageNumber, int pageSize, E exmp) {
        return new PageInfo<>(list(pageNumber, pageSize, exmp));
    }*/

    /**
     * 根据id查询单个数据
     *
     * @param id
     * @return
     */
    public T get(I id) {
        return mapper.selectByPrimaryKey(id);
    }

    /************************基础查询*************************/

    /************************基础删除*************************/
    public void del(I id) {
        mapper.deleteByPrimaryKey(id);
    }
    /************************基础删除*************************/


    /************************Service成的帮助方法*************************/
    protected <Q> Q getOnlyOne(List<Q> list) {
        if (list == null || list.size() == 0) {
            return null;
        } else {
            if (list.size() > 1) {
                throw new RuntimeException("唯一性查询出现多个结果, 请查看数据情况.");
            } else {
                return list.get(0);
            }
        }
    }

    protected void setCreateInfo(MybatisEntity entity, long userId, String username) {
        entity.setCreatorId(userId);
        entity.setCreatorName(username);
        entity.setCreateDate(new Date());
    }

    protected void setModifyInfo(MybatisEntity entity, long userId, String username) {
        entity.setModifierId(userId);
        entity.setModifierName(username);
        entity.setModifierDate(new Date());
    }



}
