package com.superui.manager.dynamic;

import java.lang.annotation.*;

/**
 * @author Bofa
 * @version 1.0
 * @decription com.ai.service.management.dynamic
 * @date 2018/9/3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface Aids {
    String value();
}
