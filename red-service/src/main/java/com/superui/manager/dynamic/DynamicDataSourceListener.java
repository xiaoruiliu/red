package com.superui.manager.dynamic;

import com.alibaba.druid.pool.DruidDataSource;
import com.superui.manager.dao.entity.DbConfig;
import com.superui.manager.dynamic.bean.DataSourceInfo;
import com.superui.manager.service.DbConfigSv;
import com.superui.manager.utils.JsonMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamicDataSourceListener implements ApplicationListener<ApplicationContextEvent> {

    public final Logger log = LoggerFactory.getLogger(DynamicDataSourceListener.class);

    @Autowired
    private DbConfigSv dbConfigSv;

    @Autowired
    private DynamicDataSource dynamicDataSource;

    private volatile boolean init = false;

    @Override
    public void onApplicationEvent( ApplicationContextEvent event ) {
        // 如果是容器刷新事件OR Start Event
        if (event instanceof ContextRefreshedEvent) {
            if (!init) {
                register();
            }
        }
    }

    private synchronized void register() {
        init = true;
        List<DbConfig> dbAcctList = dbConfigSv.listAll();

        registerDynamicBean(dbAcctList);
    }

    private void registerDynamicBean( List<DbConfig> dbAcctList ) {

        if (dbAcctList != null) {
            Map<Object, DataSource> map = new HashMap<Object, DataSource>();

            for(DbConfig dbConfig : dbAcctList){

                if(StringUtils.isBlank(dbConfig.getDbCode()) ||
                        StringUtils.isBlank(dbConfig.getDbConfig())){
                    log.error("配置的数据源 : " + dbConfig + " 不符合规范!");
                }

                DataSource ds = null;
                try{
                    DataSourceInfo dsinfo = JsonMapper.defaultMapper().fromJson(dbConfig.getDbConfig(), DataSourceInfo.class);
                    //ValidatatorUtil.validate(dsinfo);

                    ds = buildDataSource(dsinfo);

                }catch (Exception e){
                    log.error("配置的数据源 : " + dbConfig + " 不符合规范!", e);
                    continue;
                }

                if(ds != null){
                    map.put(dbConfig.getDbCode(), ds);
                }

            }
            dynamicDataSource.putDynamicDataSources(map);
        }

    }

    private DataSource buildDataSource( DataSourceInfo info){

        DruidDataSource ds = new DruidDataSource();

        /*
         * 基本属性
         */
        ds.setDriverClassName(info.getDriverClassName());
        ds.setUrl(info.getUrl());
        ds.setUsername(info.getUsername());
        ds.setPassword(info.getPassword());
        /*
         * 配置初始化大小、最小、最
         */
        ds.setInitialSize(info.getInitialSize());
        ds.setMinIdle(info.getMinIdle());
        ds.setMaxActive(info.getMaxActive());

        /*
         * 配置获取连接等待超时的时间
         */
        ds.setMaxWait(6000);

        /*
         * 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
         */
        ds.setTimeBetweenEvictionRunsMillis(60000);

        /*
         * 配置一个连接在池中最小生存的时间，单位是毫秒
         */
        ds.setMinEvictableIdleTimeMillis(300000);

        //ds.setValidationQuery("SELECT 'X' FROM DUAL");
        //ds.setTestWhileIdle(true);
        ds.setTestOnBorrow(false);
        ds.setTestOnReturn(false);

        /*
         * 打开PSCache，并且指定每个连接上PSCache的大小
         */
        ds.setPoolPreparedStatements(false);
        ds.setMaxPoolPreparedStatementPerConnectionSize(20);

        return ds;
    }

}
