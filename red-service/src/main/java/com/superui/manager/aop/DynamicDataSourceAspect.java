package com.superui.manager.aop;

import com.superui.manager.dynamic.Aids;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Bofa
 * @version 1.0
 * @decription com.ai.service.management.dynamic
 * @date 2018/9/3
 */
@Aspect
@Order(1)
@Component
public class DynamicDataSourceAspect {

    @Around("execution(* com.superui.manager.service..*Sv.*(..))")
	public Object awareDynamicDB(ProceedingJoinPoint pjp) throws Throwable {

		String dbname = getDbName(pjp);
        DynamicDataSourceContextHolder.setDataSourceName(dbname);

		try {
			return pjp.proceed();
		} catch (Throwable e) {
			throw e;
		} finally {
            DynamicDataSourceContextHolder.clearDataSourceName();
		}
	}

	private String getDbName(ProceedingJoinPoint pjp) {

		try {
			Aids db = pjp.getTarget().getClass().getAnnotation(Aids.class);

			if(db != null){
				return db.value();
			}
		} catch (Exception e) {}

		return null;
	}

}
