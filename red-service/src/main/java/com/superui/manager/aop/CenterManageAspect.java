package com.superui.manager.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Order(1)
@Component
public class CenterManageAspect {

    @Around("execution(* com.superui.manager.service..*Sv.*(..))")
    public Object awareCenter(ProceedingJoinPoint pjp) throws Throwable {

        String init = null;

        try {

            String centerInit = DynamicDataSourceContextHolder.getCenterInit();

            if(centerInit == null){
                //认为是初始化层
                centerInit = Boolean.TRUE.toString();
                init = centerInit;
                DynamicDataSourceContextHolder.setCenterInit(centerInit);
            }

            return pjp.proceed();
        } catch (Throwable e) {
            throw e;
        } finally {
            if(init != null){
                DynamicDataSourceContextHolder.clearCenterInit();
                DynamicDataSourceContextHolder.clearCenter();
            }
        }
    }

}
