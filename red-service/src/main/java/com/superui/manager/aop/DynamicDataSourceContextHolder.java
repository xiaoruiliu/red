package com.superui.manager.aop;

import com.superui.manager.exceptions.NbsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

/**
 * @author Bofa
 * @version 1.0
 * @decription com.ai.service.management.dynamic
 * @date 2018/9/3
 */
public class DynamicDataSourceContextHolder {

    private Logger logger = LoggerFactory.getLogger(DynamicDataSourceContextHolder.class);

    public static final String DEFAULT_BASE = "AI_DEFAULT_BASE_T";

    /**
     * 该设置如果使用不当, 会形成泄露, 所以需要开发人员正确处理, 有加, 必须有删
     */
    //存放当前线程使用的数据源类型信息
    private static final ThreadLocal<Stack<String>> centerHolder = new ThreadLocal<Stack<String>>(){
        @Override
        protected Stack<String> initialValue() {
            return new Stack<String>();
        }
    };

    private static final ThreadLocal<String> centerInitHolder = new ThreadLocal<String>(){
        @Override
        protected String initialValue() {
            return null;
        }
    };

    private static final ThreadLocal<Stack<String>> dataSourceNameHolder = new ThreadLocal<Stack<String>>(){
        @Override
        protected Stack<String> initialValue() {
            return new Stack<String>();
        }
    };

    /*------------------------数据源名字设置--------------------------*/
    public static void setDataSourceName(String dbname) {
        Stack<String> stack = dataSourceNameHolder.get();
        if(dbname == null){
            stack.push(DEFAULT_BASE);
        }else{
            stack.push(dbname);
        }
    }

    public static String getDataSourceName() {
        Stack<String> stack = dataSourceNameHolder.get();
        if(!stack.isEmpty()){
            return stack.peek();
        }else{
            return null;
        }
    }

    public static void clearDataSourceName() {
        Stack<String> stack = dataSourceNameHolder.get();
        if(!stack.isEmpty()){
            stack.pop();
        }
    }


    /*------------------------数据源分库属性设置--------------------------*/
    public static void setCenter(String center) {
        Stack<String> stack = centerHolder.get();
        if(center == null){
            NbsException.throwNbsException("设置的中心名为空!");
        }else{
            stack.push(center);
        }
    }

    public static String getCenter() {
        Stack<String> stack = centerHolder.get();
        if(!stack.isEmpty()){
            return stack.peek();
        }else{
            return null;
        }
    }

    public static void clearCenter() {
        Stack<String> stack = centerHolder.get();
        if(!stack.isEmpty()){
            stack.pop();
        }
    }

    /*------------------------数据源分库属性设置--------------------------*/
    static String getCenterInit() {
        return centerInitHolder.get();
    }

    static void setCenterInit( String value ) {
        centerInitHolder.set(value);
    }

    static void clearCenterInit(){
        centerInitHolder.remove();
    }
}
