package com.superui.manager.utils.redisUtils;

import com.superui.manager.exceptions.NbsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisCluster;

import java.util.function.Function;

@Component
public class RedisClient {

    private static final Logger log = LoggerFactory.getLogger(RedisClient.class);

    @Autowired
    private RedisCmpt redisCmpt;

    public String get(String uniCode,String redisUrl,String passWord, String key) {
        return this.exe(uniCode,redisUrl,passWord, e -> e.get(key));
    }

    public String hget(String uniCode,String redisUrl,String passWord, String key, String field) {
        return this.exe(uniCode,redisUrl,passWord, e -> e.hget(key, field));
    }

    public Boolean exists(String uniCode,String redisUrl,String passWord, String key) {
        return this.exe(uniCode,redisUrl,passWord, e -> e.exists(key));
    }

    // 返回miao
    public Long ttl(String uniCode,String redisUrl,String passWord, String key) {
        return this.exe(uniCode,redisUrl,passWord, e -> e.ttl(key));
    }

    // 返回毫秒
    public Long pttl(String uniCode,String redisUrl,String passWord, String key) {
        return this.exe(uniCode,redisUrl,passWord, e -> e.pttl(key));
    }

    public String set(String uniCode,String redisUrl,String passWord, String key, String value) {
        NbsException.throwNbsException("中台能力运营中心不允许写入数据库");
        return this.exe(uniCode,redisUrl,passWord, e -> e.set(key, value));
    }

    public <R> R exe(String uniCode,String redisUrl,String passWord, Function<JedisCluster, R> function) {
        JedisCluster jedisCluster = redisCmpt.getJedisCluster(uniCode,redisUrl,passWord);
        if (jedisCluster == null) {
            NbsException.throwNbsException("redis链接为空!");
        }
        try {
            return function.apply(jedisCluster);
        } catch (Exception e) {
            log.error("redis操作失败！",e);
            NbsException.throwNbsException("redis集群操作失败,操作的集群编码是" + uniCode);
        }
        return null;
    }

}
