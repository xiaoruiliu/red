package com.superui.manager.utils.httpUtils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Http发送字节流文件到报文体内
 * @author wenyn
 * @since 2020-11-11
 */
public class HttpUtil {

    /**
     * 发送文件字节流到报文体内
     * @param url
     * @param bytes
     * @return
     * @throws Exception
     */
    public static String postBytes(String url,byte[] bytes) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new ByteArrayEntity(bytes));
        httpPost.setHeader("Content-type", "application/octet-stream;charset=UTF-8");
        //这里是设置虚拟IP
        InetAddress address = InetAddress.getLocalHost();
//        String remoteIp = (int) (Math.random() * 255 + 1)+"."+(int) (Math.random() * 255 + 1)+"."+(int) (Math.random() * 255 + 1)+"."+(int) (Math.random() * 255 + 1);
        httpPost.setHeader("X-Forwarded-For",address.getHostAddress());
        CloseableHttpResponse httpResponse = HttpClients.createDefault().execute(httpPost);
        try {
            HttpEntity entityResponse = httpResponse.getEntity();
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            int contentLength = (int) entityResponse.getContentLength();
//            OutputStream out  = new ByteArrayOutputStream(contentLength);
            if (contentLength <= 0)
                throw new IOException("No response");
            int total = 0;
            int len ;
            //respBuffer是一个缓存数组，缓存InputStream.read()的数据，由于可能一次取不全，所以分多次取，最后是-1时候不取了结束了
            byte[] respBuffer = new byte[contentLength];
            byte[] respBuffer1 = new byte[contentLength];
            while ((len = entityResponse.getContent().read(respBuffer)) != -1 ){
                for (int i=0;i<len;i++){
                    respBuffer1[total+i] = respBuffer[i];
                }
                total = total + len;
            }
//            System.out.println(entityResponse.getContent().read(respBuffer,0,contentLength));

            if (total != respBuffer.length)
                throw new IOException("Read response buffer error");
            return new String(respBuffer1, "utf-8");
        } finally {
            httpResponse.close();
        }
    }

}
