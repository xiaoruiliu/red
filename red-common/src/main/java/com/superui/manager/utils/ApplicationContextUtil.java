package com.superui.manager.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ApplicationContextUtil
 * @author:
 * @date:
 * @Description:
 * spring配置文件中配置该类的bean，然后implements ApplicationContextAware
 * 加载Spring配置文件时，如果Spring配置文件中所定义的Bean类实现了ApplicationContextAware 接口，
 * 那么在加载Spring配置文件时，会自动调用ApplicationContextAware 接口中的setApplicationContext
 */
@Component
public class ApplicationContextUtil implements ApplicationContextAware{

    private static ApplicationContext applicationContext;

    /**
     * @Function: ApplicationContextAware :: setApplicationContext
     * @author: taoyf
     * @date: 2017年4月24日 下午3:36:04
     * @Description:
     * @return
     * @throws
     */
    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        applicationContext = ac;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }


}