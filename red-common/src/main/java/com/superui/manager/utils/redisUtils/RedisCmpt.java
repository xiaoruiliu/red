package com.superui.manager.utils.redisUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RedisCmpt implements Closeable {

    private static final Logger log = LoggerFactory.getLogger(RedisCmpt.class);

    private ConcurrentHashMap<String, JedisCluster> jedisClusterMap = new ConcurrentHashMap<>();

    public JedisCluster getJedisCluster(String uniCode,String redisUrl,String passWord) {
        JedisCluster jedisCluster = jedisClusterMap.get(uniCode);
        if (jedisCluster == null) {
            synchronized (RedisCmpt.class) {
                jedisCluster = jedisClusterMap.get(uniCode);
                if (jedisCluster != null) {
                    return jedisCluster;
                } else {
                    jedisCluster = createJedis(redisUrl,passWord);
                    jedisClusterMap.put(uniCode, jedisCluster);
                }
            }
        }

        return jedisCluster;
    }

    private JedisCluster createJedis(String redisUrl,String passWord) {
        Set<HostAndPort> hostAndPorts = createHostAndPort(redisUrl);
        return new JedisCluster(hostAndPorts, 1000, 1000, 2, passWord, jedisPoolConfig());
    }

    private Set<HostAndPort> createHostAndPort(String redisUrl) {
        //20.26.37.179:28001,20.26.37.179:28002
        Set<HostAndPort> hostAndPorts = new HashSet<>();
        String[] ipAndPords = redisUrl.split(",");
        for (String ipap : ipAndPords) {
            String[] ipAndPort = ipap.split(":");
            hostAndPorts.add(new HostAndPort(ipAndPort[0], Integer.parseInt(ipAndPort[1])));
        }
        return hostAndPorts;

    }

    private JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 连接数
        jedisPoolConfig.setMaxTotal(5);
        //最小空闲数
        jedisPoolConfig.setMaxIdle(1);
        //是否在从池中取出连接前进行检验，如果检验失败，则从池中去除连接并尝试取出另一个
        jedisPoolConfig.setTestOnBorrow(true);
        //在return给pool时，是否提前进行validate操作
        jedisPoolConfig.setTestOnReturn(true);
        //在空闲时检查有效性，默认false
        jedisPoolConfig.setTestWhileIdle(true);
        //表示一个对象至少停留在idle状态的最短时间，然后才能被idle object evitor扫描并驱逐；
        //这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义
        jedisPoolConfig.setMinEvictableIdleTimeMillis(30000L);
        //表示idle object evitor两次扫描之间要sleep的毫秒数
        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(60000L);
        //表示idle object evitor每次扫描的最多的对象数
        jedisPoolConfig.setNumTestsPerEvictionRun(1000);
        //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
        jedisPoolConfig.setMaxWaitMillis(15 * 1000);

        return jedisPoolConfig;
    }

    @PreDestroy
    @Override
    public void close() throws IOException {
        if (jedisClusterMap != null) {
            for (Map.Entry<String, JedisCluster> jedisClusterEntry : jedisClusterMap.entrySet()) {
                if (jedisClusterEntry.getValue() != null) {
                    try {
                        jedisClusterEntry.getValue().close();
                    } catch (Exception e) {
                        log.error("redis链接关闭失败,不影响操作", e);
                    }
                }
            }
            jedisClusterMap.clear();
        }
    }


}
