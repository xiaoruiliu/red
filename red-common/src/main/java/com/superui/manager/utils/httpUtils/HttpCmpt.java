package com.superui.manager.utils.httpUtils;

import com.alibaba.druid.support.json.JSONUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class HttpCmpt {

    private static final String DEFAULT_ENCODING = "utf-8";

    private static final Logger logger = LoggerFactory.getLogger(HttpCmpt.class);

//    @Autowired
//    private StaticDataConfigSv staticDataConfigSv;

    /**
     * get请求
     *
     * @return
     */
    public String doGet(String url) {

        logger.error("调用url{}", url);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        try {

            httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);

            response = httpclient.execute(httpGet);

            /**请求发送成功，并得到响应**/
            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }

        } catch (IOException e) {
            logger.error("get url(" + url + ") 异常!", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }

        return null;
    }

    /**
     * get请求
     *
     * @return
     */
    public String doGetException(String url) {

        logger.error("调用url{}", url);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        try {

            httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);

            response = httpclient.execute(httpGet);

            /**请求发送成功，并得到响应**/
            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }

        } catch (IOException e) {
            logger.error("get url(" + url + ") 异常!", e);
            throw new RuntimeException("get url(" + url + ") 异常!" + e.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }

        return null;
    }

    /**
     * get请求
     *
     * @return
     */
    public String doGetParamMap(String url, Map<String, Object> param) {

        logger.error("调用url{}", url);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        if (null != param && !param.isEmpty()) {
            StringBuilder append = new StringBuilder();
            for (String key : param.keySet()) {
                append.append("&").append(key).append("=").append(param.get(key));
            }

            url += "?" + append.substring(1);
        }

        try {

            httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);

            response = httpclient.execute(httpGet);

            /**请求发送成功，并得到响应**/
            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }

        } catch (IOException e) {
            logger.error("get url(" + url + ") 异常!", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }

        return null;
    }

    /**
     * post请求(用于key-value格式的参数)
     *
     * @param url
     * @param params
     * @return
     */
    public String doPost(String url, Map params) {
        logger.error("调用url{},报文:{}", url, params);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        try {

            httpclient = HttpClients.createDefault();
            // 实例化HTTP方法
            HttpPost request = new HttpPost();
            request.setURI(new URI(url));

            //设置参数
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
                String name = (String) iter.next();
                String value = String.valueOf(params.get(name));
                nvps.add(new BasicNameValuePair(name, value));
            }

            request.setEntity(new UrlEncodedFormEntity(nvps));

            response = httpclient.execute(request);

            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }
        } catch (Exception e) {
            logger.error("post url(" + url + ") params(" + params + ") 异常!", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }

        return null;
    }


    /**
     * post请求（用于请求json格式的参数）
     *
     * @param url
     * @param json
     * @return
     */
    public String doPostJson(String url, String json) throws Exception {
        logger.error("调用url{},报文:{}", url, json);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;
        try {

            httpclient = HttpClients.createDefault();

            HttpPost httpPost = new HttpPost(url);// 创建httpPost

            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json, DEFAULT_ENCODING);
            httpPost.setEntity(entity);

            response = httpclient.execute(httpPost);

            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }

        } catch (Exception e) {
            logger.error("post url(" + url + ") json(" + json + ") 异常!", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }
        return null;
    }

    public String doPostJsonException(String url, String json) {
        return doPostJsonException(url, json, 60000, 10000, 10000);
    }

    public String doPostJsonException(String url, String json, int socketTimeout, int connectTimeout, int connectionRequestTimeout) {

        logger.error("调用url:{},报文:{}", url, json);

        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        try {

            httpclient = HttpClients.createDefault();

            HttpPost httpPost = new HttpPost(url);// 创建httpPost

//            StaticDataConfig flagConfig = staticDataConfigSv.get("BATCHSYNCGW", "X-FORWARDED-FOR", "FLAG");
//            if (StringUtils.contains(url, "/opr/batchSync")
//                    && Objects.nonNull(flagConfig)
//                    && StringUtils.equals("Y", flagConfig.getValue1())) {
//                //这里是设置虚拟IP
//                InetAddress address = InetAddress.getLocalHost();
//                if (Objects.nonNull(address)) {
//                    httpPost.setHeader("X-Forwarded-For", address.getHostAddress());
//                    logger.error("调用/opr/batchSync的主机ip为[" + address.getHostAddress() + "]");
//                }
//                if (StringUtils.isNotBlank(flagConfig.getValue2())) {
//                    httpPost.setHeader("X-Forwarded-For", flagConfig.getValue2());
//                    logger.error("存在指定ip配置，则调用/opr/batchSync的主机ip为指定ip[" + flagConfig.getValue2() + "]");
//                }
//            }
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/json");
            RequestConfig config = RequestConfig.custom().setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).setConnectionRequestTimeout(connectionRequestTimeout).build();
            httpPost.setConfig(config);
            StringEntity entity = new StringEntity(json, DEFAULT_ENCODING);
            httpPost.setEntity(entity);

            response = httpclient.execute(httpPost);

            if (response != null) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }

        } catch (Exception e) {
            logger.error("post url(" + url + ") json(" + json + ") 异常!{}", e);
            throw new RuntimeException("post url(" + url + ") json(" + json + ") 异常!" + e.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }
        return null;
    }

    /**
     * post请求通过application/x-www-form-urlencoded请求传递json
     *
     * @param url
     * @param params
     * @return
     */
    public String doFormJson(String url, Map params) {
        logger.error("调用url{},报文:{}", url, params);
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        try {

            httpclient = HttpClients.createDefault();
            // 实例化HTTP方法
            HttpPost request = new HttpPost();
            request.setURI(new URI(url));
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            //设置参数

            String json = JSONUtils.toJSONString(params);
            HttpEntity entity = new StringEntity(json);
            request.setEntity(entity);

            response = httpclient.execute(request);

            if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String res = EntityUtils.toString(response.getEntity(), DEFAULT_ENCODING);
                logger.error("返回的报文:{}", res);
                return res;
            }
        } catch (Exception e) {
            logger.error("post url(" + url + ") params(" + params + ") 异常!", e);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
            if (httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException e) {
                    logger.error("关闭失败", e);
                }
            }
        }

        return null;
    }

    public String doPostWithHeader(String url, String json, Header[] headers) {

        CloseableHttpClient httpClient = null;
        CloseableHttpResponse execute = null;
        logger.error("请求地址{},请求头:{},请求内容:{}", url, headers, json);

        try {
            httpClient = HttpClients.createDefault();

            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeaders(headers);
//            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/json");
            RequestConfig config = RequestConfig.custom().setConnectTimeout(10000).setSocketTimeout(60000).setConnectionRequestTimeout(10000).build();
            httpPost.setConfig(config);
            StringEntity entity = new StringEntity(json, DEFAULT_ENCODING);
            httpPost.setEntity(entity);
            long startTime = System.currentTimeMillis();
            execute = httpClient.execute(httpPost);
            long endTime = System.currentTimeMillis();
            logger.error("请求地址{}，本次调用耗时为{}", url, (endTime - startTime));

            if (execute != null && execute.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String s = EntityUtils.toString(execute.getEntity());
                logger.error("返回的报文:{}", s);
                return s;
            } else if (execute != null) {
                logger.error("返回的请求HttpStatus是:{},返回内容是:{}", execute.getStatusLine().getStatusCode(), execute.getEntity());
            } else {
                logger.error("返回的请求为null,请求地址{},请求头:{},请求内容:{}", url, headers, json);
            }
        } catch (Exception e) {
            logger.error("post url({}),header参数:{}, json({}) 异常!", url, headers, json, e);
        } finally {
            try {
                if (execute != null) {
                    execute.close();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (Exception e) {
                logger.error("关闭失败", e);
            }
        }
        return null;
    }

}