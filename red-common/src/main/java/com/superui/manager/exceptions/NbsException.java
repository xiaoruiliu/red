package com.superui.manager.exceptions;

import org.springframework.stereotype.Component;

public class NbsException extends RuntimeException {
    private static final long serialVersionUID = 4242111829918405178L;

    public NbsErrorCode errorCode;

    public NbsException(String message) {
        super(message);
    }

    public NbsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NbsException(NbsErrorCode errorCode, String... message) {
        super(errorCode.getMessageAndCompletion(message));
        this.errorCode = errorCode;
    }

    public static void throwNbsException(String message){
        throw new NbsException(message);
    }

    public static void throwNbsException(NbsErrorCode errorCode, String... message){
        throw new NbsException(errorCode, message);
    }

    public static void throwNbsException(String message, Throwable cause){
        throw new NbsException(message, cause);
    }

}
